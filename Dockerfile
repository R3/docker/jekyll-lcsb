FROM ruby:2.6
MAINTAINER Jacek Lebioda "jacek.lebioda@uni.lu"
RUN gem update --system && \
    gem install bundler && \
    gem install sassc -- --disable-march-tune-native
COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install && bundle update
