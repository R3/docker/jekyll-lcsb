# Baseimage for Jekyll@LCSB
A Docker base-image containing Ruby dependencies for Jekyll instances using Jekyll-LCSB-Theme.

# Versions
|   Tag             | Jekyll version | Bundler version | Theme version | Additional |
|:-----------------:|:--------------:|-----------------|---------------|------------|
| 1.8 (or latest)   |   Jekyll 4.2.0 | 2.1.4           | 0.3.18        | Spaceship  |
| 1.7               |   Jekyll 4.1.1 | 2.1.4           | 0.3.18        | Spaceship  |
| 1.6               |   Jekyll 4.0.0 | 2.1.4           | 0.3.18        | Spaceship  |
| 1.5               |   Jekyll 4.0.0 | 2.1.4           | 0.3.18        |            |
| 1.4               |   Jekyll 4.0.0 | 2.0.2           | 0.3.6         |            |
| 1.3               |   Jekyll 4.0.0 | 2.0.2           | 0.3.3         |            |
| 1.2               |   Jekyll 4.0.0 | 2.0.2           | 0.3.3         |            |
| 1.1               |   Jekyll 4.0.0 | 2.0.2           | 0.3.3         |            |
| 1.0               |   Jekyll 4.0.0 | 2.0.2           | 0.3.0         |            |
| 0.1 (or legacy)   |   Jekyll 3.8.6 | 1.17.3          | 0.3.0         |            |

# How to use? (for developers of applications using the image)
In your `.gitlab-ci.yml` file add the following line (make sure `r3` is written in lower-case):

```
image: git-r3lab.uni.lu:4567/r3/docker/jekyll-lcsb:latest

# or, if you want to use legacy (or other versions) 
image: git-r3lab.uni.lu:4567/r3/docker/jekyll-lcsb:0.1
```

For the reproducibility, it's good if you pin the version to a specific tag, e.g. `1.3` instead of `latest`.

# How to update this repository (for developers of this repository)
Locally, change `Gemfile` and run `bundle update`. Commit `Gemfile.lock`, and tag the commit.

# How to build and push to the repository manually? (for developers of this repository)
Normally it is handled by gitlab-CI automatically (see `.gitlab-ci.yml`) when you tag the commit.

Anyway, if you'd like to do it yourself, perform these steps:

Login to gitlab registry (you can generate the token to login with https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

```
docker login git-r3lab.uni.lu:4567
```

Then, build the image:

```
# replace `new_tag` with e.g. 1.3, and don't forget the dot...
docker build -t git-r3lab.uni.lu:4567/r3/docker/jekyll-lcsb:new_tag .  # ...there
docker build -t git-r3lab.uni.lu:4567/r3/docker/jekyll-lcsb:latest .
```

Publish in repository:

```
docker push git-r3lab.uni.lu:4567/r3/docker/jekyll-lcsb
```
